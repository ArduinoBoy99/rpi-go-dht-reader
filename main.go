// main
package main

import (
	"crypto/tls"
	"encoding/json"
	"flag"
	"fmt"
	"os"
	"os/signal"
	"time"

	"github.com/MichaelS11/go-dht"
	MQTT "github.com/eclipse/paho.mqtt.golang"
)

const (
	mqtt_qos           int    = 0
	mqtt_retained      bool   = false
	standard_mqtt_port string = "1883"
	dht11Pin           int    = 3

	sensor_id_dht11           string = "DHT11"
	sensor_id_dht22           string = "DHT22"
	sensor_value_type_digital string = "digital"
	sensor_type_temperature   string = "temperature"
)

type Application struct {
	mqttTopic  string
	mqttBroker string

	SenderId   string
	SenderType string

	SleepTime int64

	ClientMQTT MQTT.Client
	TlsConfig  *tls.Config
	ConnOpts   *MQTT.ClientOptions
	Dht        *dht.DHT
	Run        bool
}

type SensorsMessage struct {
	SenderId   string
	SenderType string
	Timestamp  string
	Data       []*SensorData
}

type SensorData struct {
	SensorId   string
	SensorType string
	Value      float64
	ValueType  string
}

func loadAppConfig() (out *Application) {
	topic := flag.String("topic", "test/sensorsData", "MQTT topic for messages.")
	brokerIP := flag.String("brokerIP", "192.168.0.106", "MQTT broker IP.")
	senderID := flag.String("senderId", "99", "Id of device which will be generating sensor messages.")
	senderType := flag.String("senderType", "RPiZeroW", "Type of device which will be generating sensor messages.")
	sleepT := flag.Int64("sleep", 2, "For how long in seconds should device sleep between sending data messages.")
	flag.Parse()

	out = &Application{
		Run:        true,
		mqttTopic:  *topic,
		mqttBroker: *brokerIP + ":" + standard_mqtt_port,
		SenderId:   *senderID,
		SenderType: *senderType,
		SleepTime:  *sleepT,
	}

	fmt.Println("Loaded configuration:", *out)

	return out
}

func (app *Application) createConfig() {
	app.TlsConfig = &tls.Config{InsecureSkipVerify: true, ClientAuth: tls.NoClientCert}
	app.ConnOpts = MQTT.NewClientOptions().AddBroker(app.mqttBroker).SetClientID(app.SenderId).SetCleanSession(true)
	app.ConnOpts.SetTLSConfig(app.TlsConfig)
	app.ClientMQTT = MQTT.NewClient(app.ConnOpts)
}

func (app *Application) connectToMQTTBroker() {
	token := app.ClientMQTT.Connect()
	if token.Wait() && token.Error() != nil {
		fmt.Println("Error on creating token. Error:", token.Error())
		return
	}
	fmt.Printf("Connected to %s.\n", app.mqttBroker)
}

func (app *Application) mainGoroutine() {
	for app.Run {
		dht11Data := app.readDhtData()

		fmt.Println("Sending message:", time.Now().UTC().Format("2006-01-02 15:04:05.999"))
		message := &SensorsMessage{
			SenderId:   app.SenderId,
			SenderType: app.SenderType,
			Timestamp:  time.Now().UTC().Format("2006-01-02 15:04:05.999"),
			Data:       dht11Data,
		}

		jsonMsg, err := json.Marshal(message)
		if err != nil {
			fmt.Println("Error on marshalling message. Error:", err)
			return
		}

		app.ClientMQTT.Publish(app.mqttTopic, byte(mqtt_qos), mqtt_retained, jsonMsg)
		fmt.Println("Message sent.")

		time.Sleep(time.Duration(app.SleepTime) * time.Second)
	}
}

func (app *Application) start() {
	err := dht.HostInit()
	if err != nil {
		fmt.Println("HostInit error:", err)
		return
	}

	app.Dht, err = dht.NewDHT("GPIO22", dht.Celsius, "")
	if err != nil {
		fmt.Println("NewDHT error:", err)
		return
	}
	go app.mainGoroutine()
}

func (app *Application) readDhtData() (out []*SensorData) {
	out = make([]*SensorData, 2)

	humidity, temperature, err := app.Dht.ReadRetry(11)
	if err != nil {
		fmt.Println("Read error:", err)
		return out
	}

	tempData := &SensorData{
		SensorId:   sensor_id_dht11,
		SensorType: sensor_type_temperature,
		Value:      temperature,
		ValueType:  sensor_value_type_digital,
	}

	humData := &SensorData{
		SensorId:   sensor_id_dht11,
		SensorType: sensor_type_temperature,
		Value:      humidity,
		ValueType:  sensor_value_type_digital,
	}

	out[0] = tempData
	out[1] = humData
	return out
}

func (app *Application) waitForShutdown(shutdownChannel chan bool) {
	<-shutdownChannel
	app.Run = false
	time.Sleep(time.Second * time.Duration(6))
	return
}

func main() {
	fmt.Println("Starting application..")
	channelShutdown := make(chan bool)
	c_signal := make(chan os.Signal, 1)

	signal.Notify(c_signal, os.Interrupt)
	signal.Notify(c_signal, os.Kill)
	go func() {
		<-c_signal
		channelShutdown <- true
		fmt.Println("Shutting down application..")
		os.Exit(1)
	}()

	app := loadAppConfig()
	app.createConfig()
	app.connectToMQTTBroker()
	app.start()
	app.waitForShutdown(channelShutdown)
}
