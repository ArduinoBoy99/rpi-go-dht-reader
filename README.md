# RPi-Go-DHT-Reader

This Go program reads data from DHT 
and sends that data to mosquitto message broker.
IP address of mosquitto, senderID, senderType and 
mosquitto topic can be set via command line flags when 
launching application. If not set, defaults will be used.

This application can be used to build network of small
RPiZero devices with DHT or, if code is modified, any
other sensor. Data is then being sent to message broker.

Other devices can subscribe to that data and analyze it
for further processing and/or storage.

# Flags
- topic (if it doesn't exists it will be created)
- brokerIP (where mosquitto is located at)
- senderId (unique identification of device sending this data)
- senderType (device sending the data can be categorized under types)
- sleep(device sleep between sending data messages)

# Data structures
```
type SensorsMessage struct {
        SenderId   string //id of device sending the data
        SenderType string //type of device sending the data
        Timestamp  string //message timestamp in string format
        Data       []*SensorData //array of sensor data messages
}
```

```
type SensorData struct {
        SensorId   string //unique id of a sensor on a device
        SensorType string //type of a sensor
        Value      float64 //sensor reading value
        ValueType  string //type of value(analog, digital, boolean etc.)
}
```

